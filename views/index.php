<!DOCTYPE html>

<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Registro de usuario</title>
    <link rel="stylesheet" type="text/css" href="./public/css/spoty.css">
</head>
<body> 
    <div class="title">Registro de usuario</div>
	<form action="Usuario/create<?php echo LOGICGET; ?>" method="POST" enctype="multipart/form-data" name="">
		<input name="username" placeholder="Usuario" type="text">
		<input name="password" placeholder="Clave" type="password">
		<input name="avatar" type="file">
		<input type="submit">
	</form>
    <div class="title">Registro de Canción</div>
	<form action="Canciones/create<?php echo LOGICGET; ?>" method="POST" enctype="multipart/form-data" name="">
		<input name="title" placeholder="Titulo" type="text">
		<input name="author" placeholder="Autor" type="text">
		<input name="duration" placeholder="Duración (s)" type="text">
		<input name="format" placeholder="Formato" type="text">
		<select name="idAlbum">
			<option value="1">Tales</option>
		</select>
		<input name="file" type="file">
		<input type="submit">
	</form>
</body>
</html>