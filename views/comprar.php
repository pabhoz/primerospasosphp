<!DOCTYPE html>

<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Registro de usuario</title>
    <link rel="stylesheet" type="text/css" href="./public/css/spoty.css">
    <script src="<?php echo PUBLICO; ?>js/jquery-1.12.4.js"></script>
</head>
<body> 
    <div class="title">Comprar canciones</div>
	<form id="comprar">
            <select name="usuario">
                <option value="" selected="true">-Seleccione usuario</option>
            </select>
            
            <select name="cancion">
                <option selected="true">-Seleccione canción-</option>
            </select>
            <input type="submit" value="comprar">
	</form>
    
    <div class="title">Canciones de un usuario</div>
	<form action="registroUsuario<?php echo LOGICGET; ?>" method="POST" enctype="multipart/form-data" name="">
            <select>
                <option selected="true">-Seleccione usuario</option>
            </select>
            
            Aquí hay una lista de canciones
	</form>
    
    <script>
        
           $(function(){

               
               getUsers();
               getSongs();
               
               $("#comprar").submit(function(e){
                   e.preventDefault();
                   
                   var userId = $("#comprar select[name=usuario]").val();
                   var songId = $("#comprar select[name=cancion]").val();
                   
                   $.ajax({
                        url:"Usuario/comprar<?php echo LOGICGET; ?>",
                        method:"POST",
                        data: {id:userId,sid:songId}
                    }).done(function(r){
                        if(r == "1"){
                            alert("Yey gracias por comprar!");
                        }else{
                            alert("Pinche error en el sistema");
                        }
                    });
               });
               
               
           });
        
            function getUsers(){
                $.ajax({
                   url:"Usuario/getAll/true<?php echo LOGICGET; ?>",
                   method:"GET"
               }).done(function(r){
                   var response = JSON.parse(r);
                   console.log(response);
                   
                   var select = $("#comprar select[name=usuario]");
                   
                   for(var i = 0; i < response.length; i++){
                    select.append("<option value='"+response[i].id+"'>"+response[i].username+"</option>");
                   }
               });
            }
            
            function getSongs(){
                $.ajax({
                   url:"Canciones/getAll/true<?php echo LOGICGET; ?>",
                   method:"GET"
               }).done(function(r){
                   var response = JSON.parse(r);
                   console.log(response);
                   
                   var select = $("#comprar select[name=cancion]");
                   
                   for(var i = 0; i < response.length; i++){
                    select.append("<option value='"+response[i].id+"'>"+response[i].title+" - "+response[i].author+"</option>");
                   }
               });
            }
    </script>
</body>
</html>