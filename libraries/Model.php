<?php

class Model {
    
     protected static $table;

     public function getConnection(){
         return $db = new MySQLiManager("127.0.0.1", "root", "", "spotistiam");
     }
     
     public function save(){
        
         $values = $this->toArray();

         $db = self::getConnection();
         return $db->insert(static::$table,$values);
                 
     }
     
     public static function selectAll(){

         $db = self::getConnection();
         return $db->select("*",static::$table);
                 
     }
     
    public function toArray(){
        $arr = [];
        foreach ($this->getMyVars() as $key => $value) {
            $arr[$key] = $this->{"get".$key}();
        }
        return $arr;
    }
    
    public function getById($id,$table){
        $db = self::getConnection();
        $smth = $db->select("*",$table,"id = ".$id);
        return $smth;
    }

     
    
}
