<?php

class Song extends Model{
        
        protected static $table = "Song";
    
	private $id;
        private $title;
        private $author;
        private $duration;
        private $format;
        private $idAlbum;
        private $file;
        
        function __construct($id, $title, $author, $duration, $format, $idAlbum, $file) {
            $this->id = $id;
            $this->title = $title;
            $this->author = $author;
            $this->duration = $duration;
            $this->format = $format;
            $this->idAlbum = $idAlbum;
            $this->file = $file;
        }

        public function getMyVars(){
            return get_object_vars($this);
        }
        
        static function getTable() {
            return self::$table;
        }

        function getId() {
            return $this->id;
        }

        function getTitle() {
            return $this->title;
        }

        function getAuthor() {
            return $this->author;
        }

        function getDuration() {
            return $this->duration;
        }

        function getFormat() {
            return $this->format;
        }

        function getIdAlbum() {
            return $this->idAlbum;
        }

        function getFile() {
            return $this->file;
        }

        static function setTable($table) {
            self::$table = $table;
        }

        function setId($id) {
            $this->id = $id;
        }

        function setTitle($title) {
            $this->title = $title;
        }

        function setAuthor($author) {
            $this->author = $author;
        }

        function setDuration($duration) {
            $this->duration = $duration;
        }

        function setFormat($format) {
            $this->format = $format;
        }

        function setIdAlbum($idAlbum) {
            $this->idAlbum = $idAlbum;
        }

        function setFile($file) {
            $this->file = $file;
        }

        function getById($id) {
            $song = parent::getById($id, "song")[0];
            return new self($song["id"],$song["title"],$song["author"],$song["duration"],$song["format"],$song["idAlbum"],$song["file"]);
        }

}