-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema spotistiam
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `spotistiam` ;

-- -----------------------------------------------------
-- Schema spotistiam
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `spotistiam` DEFAULT CHARACTER SET utf8 ;
USE `spotistiam` ;

-- -----------------------------------------------------
-- Table `spotistiam`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `spotistiam`.`User` ;

CREATE TABLE IF NOT EXISTS `spotistiam`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `avatar` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `spotistiam`.`Song`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `spotistiam`.`Song` ;

CREATE TABLE IF NOT EXISTS `spotistiam`.`Song` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `author` VARCHAR(45) NOT NULL,
  `duration` VARCHAR(45) NOT NULL,
  `format` VARCHAR(45) NOT NULL,
  `idAlbum` INT NOT NULL,
  `file` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `spotistiam`.`Playlist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `spotistiam`.`Playlist` ;

CREATE TABLE IF NOT EXISTS `spotistiam`.`Playlist` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `owner` INT NOT NULL,
  `privacity` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX `fk_Playlist_User1_idx` (`owner` ASC),
  CONSTRAINT `fk_Playlist_User1`
    FOREIGN KEY (`owner`)
    REFERENCES `spotistiam`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `spotistiam`.`Playlist_has_Song`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `spotistiam`.`Playlist_has_Song` ;

CREATE TABLE IF NOT EXISTS `spotistiam`.`Playlist_has_Song` (
  `Playlist_id` INT NOT NULL,
  `Song_id` INT NOT NULL,
  PRIMARY KEY (`Playlist_id`, `Song_id`),
  INDEX `fk_Playlist_has_Song_Song1_idx` (`Song_id` ASC),
  INDEX `fk_Playlist_has_Song_Playlist_idx` (`Playlist_id` ASC),
  CONSTRAINT `fk_Playlist_has_Song_Playlist`
    FOREIGN KEY (`Playlist_id`)
    REFERENCES `spotistiam`.`Playlist` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Playlist_has_Song_Song1`
    FOREIGN KEY (`Song_id`)
    REFERENCES `spotistiam`.`Song` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `spotistiam`.`User_has_Song`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `spotistiam`.`User_has_Song` ;

CREATE TABLE IF NOT EXISTS `spotistiam`.`User_has_Song` (
  `User_id` INT NOT NULL,
  `Song_id` INT NOT NULL,
  PRIMARY KEY (`User_id`, `Song_id`),
  INDEX `fk_User_has_Song_Song1_idx` (`Song_id` ASC),
  INDEX `fk_User_has_Song_User1_idx` (`User_id` ASC),
  CONSTRAINT `fk_User_has_Song_User1`
    FOREIGN KEY (`User_id`)
    REFERENCES `spotistiam`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_Song_Song1`
    FOREIGN KEY (`Song_id`)
    REFERENCES `spotistiam`.`Song` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
