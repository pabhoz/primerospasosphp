<?php

class CancionesLogic{
    
    public function holi(){
        echo "HOLi!";
    }
    
    public function getAll($json = false){
        
        $songs = Song::selectAll();
        
        if($json){
            echo json_encode($songs);
        }else{
            return $songs;
        }
        
    }
    
    public function create(){
        
        $song = new Song(NULL, $_POST["title"], $_POST["author"], $_POST["duration"],
        $_POST["format"], $_POST["idAlbum"],null);

        $file = File::upload($_FILES["file"], "songs");

        if($file != false){

            $song->setFile($file);
            $r = $song->save();

            if($r){
                echo "<script>alert('Buena, habeis gozado con la canciòn ".$_POST["title"]."');</script>";
            }

        }
        
    }
    
}