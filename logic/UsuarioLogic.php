<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsuarioLogic
 *
 * @author Multimedia
 */
class UsuarioLogic {
    
    
    public function create(){
        
        $usr = new User(null, $_POST["username"], $_POST["password"]);
        $usr->save();

        
    }
    
    public function getAll($json = false){
        
        $users = User::selectAll();
        
        if($json){
            echo json_encode($users);
        }else{
            return $users;
        }
        
    }
    
    public function comprar(){
        
        $usr = User::getById($_POST["id"]);
       
        $song = Song::getById($_POST["sid"]);
        
        $r = $usr->comprar($song);
        
        echo $r;
        
        
    }
}
