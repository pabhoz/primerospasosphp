<?php

require "config.php";

spl_autoload_register(function ($clase) {
    if(file_exists('./models/' . $clase . '.php')){
        include './models/' . $clase . '.php';
    }elseif(file_exists('./libraries/' . $clase . '.php')){
        include './libraries/' . $clase . '.php';
    }else{
        exit("Clase ".$class." no encontrada");
    }
});

if(!isset($_GET["logic"])){
    $view = (isset($_GET["url"])) ? VIEWS.$_GET["url"].".php" :  VIEWS."index.php";

    if(file_exists($view)){
        require $view;
    }else{
        require VIEWS."404.html";
    }
}else{
    $url = (isset($_GET["url"])) ? $_GET["url"] :  "index";
    $url = explode("/", $url);
    
    $logic = $url[0]."Logic";
    
    if(file_exists( LOGIC.$logic.".php" )){
        require LOGIC.$logic.".php";
        $logic = new $logic();
        
        if(isset($url[1])){
            if($url[1] != "" || $url[1] != null){
                if(method_exists($logic, $url[1])){
                
                    if(isset($url[2])){
                        $logic->{$url[1]}($url[2]);
                    }else{
                        $logic->{$url[1]}();
                    }
                    
                }else{
                    die("No existe el metodo");
                }
            }else{
                die("El metodo no puede ser vacio");
            }
        }else{
            die("No method defined");
        }
    }else{
        require VIEWS."logic404.html";
    }
    
    
    
    
    /*
    
    
    if(file_exists($logic)){
        require $logic;
    }else{
        require VIEWS."logic404.html";
    }*/
}